﻿using inventory.Model;

namespace inventory.Services
{
    public interface ICategoryService
    {
        List<Category> GetCategory();
        Category GetCategory(string id);
        Category CreateCategory(Category category);
        void UpdateCategory(string id, Category category);
        void RemoveCategory(string id);
    }
}
