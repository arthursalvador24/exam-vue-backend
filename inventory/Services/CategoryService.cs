﻿using inventory.Model;
using MongoDB.Driver;

namespace inventory.Services
{
    public class CategoryService:ICategoryService
    {
        private readonly IMongoCollection<Category> _category;

        public CategoryService(IDatabaseSettings settings, IMongoClient mongoClient)
        {
            var database = mongoClient.GetDatabase(settings.DatabaseName);
            _category = database.GetCollection<Category>(settings.CategoryCollectionName);
        }
        //Category
        public Category CreateCategory(Category category)
        {
            _category.InsertOne(category);
            return category;
        }
        public List<Category> GetCategory()
        {
            return _category.Find(category => true).ToList();
        }
        public Category GetCategory(string id)
        {
            return _category.Find(category => category.Id == id).FirstOrDefault();
        }

        public void RemoveCategory(string id)
        {
            _category.DeleteOne(category => category.Id == id);
        }

        public void UpdateCategory(string id, Category category)
        {
            _category.ReplaceOne(category => category.Id == id, category);
        }
    }
}
