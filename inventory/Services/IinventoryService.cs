﻿using inventory.Model;
namespace inventory.Services
{
    public interface IinventoryService
    {
        List<Product> Get();
        Product Get(string id);
        Product Create(Product product);
        void Update(string id, Product product);
        void Remove(string id);


    }
}
