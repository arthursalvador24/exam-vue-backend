﻿using inventory.Model;
using MongoDB.Driver;
namespace inventory.Services
{
    public class InventoryService:IinventoryService
    {
        private readonly IMongoCollection<Product> _product;
        private readonly IMongoCollection<Category> _category;

        public InventoryService(IDatabaseSettings settings, IMongoClient mongoClient) {
            var database = mongoClient.GetDatabase(settings.DatabaseName);
            _product = database.GetCollection<Product>(settings.ProductCollectionName);
            _category = database.GetCollection<Category>(settings.CategoryCollectionName);
        }
        public Product Create(Product product)
        {
            _product.InsertOne(product);
            return product;
        }


        public List<Product> Get()
        {
            return _product.Find(product => true).ToList();
        }
        public Product Get(string id)
        {
            return _product.Find(product => product.Id == id).FirstOrDefault();
        }

        public void Remove(string id)
        {
             _product.DeleteOne(product => product.Id == id);
        }

        public void Update(string id, Product product)
        {
            _product.ReplaceOne(product => product.Id == id, product);

        }
        
    }
}
