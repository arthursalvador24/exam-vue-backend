﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace inventory.Model
{
    [BsonIgnoreExtraElements]
    public class Product
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = string.Empty;
        [BsonElement("product_name")]
        public string product_name { get; set; } = string.Empty;
        [BsonElement("sku")]
        public string sku { get; set; } = string.Empty;
        [BsonElement("description")]
        public string description { get; set; } = string.Empty;
        [BsonElement("category")]
        public string category { get; set; } = string.Empty;
        [BsonElement("cost")]
        public Double cost { get; set; }
        [BsonElement("selling_price")]
        public Double selling_price { get; set; }
        [BsonElement("tags")]
        public string tags { get; set; } = string.Empty;
    }
}
