﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace inventory.Model
{
    [BsonIgnoreExtraElements]
    public class Category
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = string.Empty;
        [BsonElement("name")]
        public string category_name { get; set; } = string.Empty;
        [BsonElement("createdBy")]
        public string createdBy { get; set; } = string.Empty;
        [BsonElement("createdDate")]
        public string createdDate { get; set; } = string.Empty;
    }
}
