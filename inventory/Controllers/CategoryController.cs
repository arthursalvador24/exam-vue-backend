﻿using inventory.Model;
using inventory.Services;
using Microsoft.AspNetCore.Mvc;

namespace inventory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService inventoryService;

        public CategoryController(ICategoryService inventoryService)
        {
            this.inventoryService = inventoryService;
        }
        //Category

        // GET: api/<InventoryController>
        [HttpGet]
        public ActionResult<List<Category>> GetCategory()
        {
            return inventoryService.GetCategory();
        }

        // GET api/<InventoryController>/5
        [HttpGet("{id}")]
        public ActionResult<Category> GetCategory(string id)
        {
            var category = inventoryService.GetCategory(id);
            if (category == null)
            {
                return NotFound($"category {id} not found");
            }
            return category;
        }

        // POST api/<InventoryController>
        [HttpPost]
        public ActionResult<Category> PostCategory([FromBody] Category category)
        {
            inventoryService.CreateCategory(category);
            return CreatedAtAction(nameof(GetCategory), new { id = category.Id }, category);
        }

        // PUT api/<InventoryController>/5
        [HttpPut("{id}")]
        public ActionResult PutCategory(string id, [FromBody] Category Category)
        {
            var category = inventoryService.GetCategory(id);
            if (category == null)
            {
                return NotFound($"Product {id} not found");
            }
            inventoryService.UpdateCategory(id, Category);
            return NoContent();
        }

        // DELETE api/<InventoryController>/5
        [HttpDelete("{id}")]
        public ActionResult DeleteCategory(string id)
        {
            var category = inventoryService.GetCategory(id);
            if (category == null)
            {
                return NotFound($"Category {id} not found");
            }
            inventoryService.RemoveCategory(id);
            return Ok($"Category Id={id} Deleted");
        }
    }
}
