﻿using inventory.Model;
using inventory.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace inventory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly IinventoryService inventoryService;

        public InventoryController(IinventoryService inventoryService) {
            this.inventoryService = inventoryService;
        }

        // GET: api/<InventoryController>
        [HttpGet]
        public ActionResult<List<Product>> Get()
        {
            return inventoryService.Get();
        }

        // GET api/<InventoryController>/5
        [HttpGet("{id}")]
        public ActionResult<Product> Get(string id)
        {
            var product = inventoryService.Get(id);
            if(product == null)
            {
                return NotFound($"Product {id} not found");
            }
            return product;
        }

        // POST api/<InventoryController>
        [HttpPost]
        public ActionResult<Product> Post([FromBody] Product product)
        {
            inventoryService.Create(product);
            return CreatedAtAction(nameof(Get), new { id = product.Id }, product);
        }

        // PUT api/<InventoryController>/5
        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Product Product)
        {
            var product = inventoryService.Get(id);
            if(product == null)
            {
                return NotFound($"Product {id} not found");
            }
            inventoryService.Update(id, Product);
            return NoContent();
        }

        // DELETE api/<InventoryController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var product = inventoryService.Get(id);
            if (product == null)
            {
                return NotFound($"Product {id} not found");
            }
            inventoryService.Remove(id);
            return Ok($"Product Id={id} Deleted");
        }



    }
}
